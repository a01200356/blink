# -*- coding: utf-8 -*-
from sys import stdout
from glob import glob
from PIL import Image
from pickle import dump
from logging import basicConfig, DEBUG

from numpy import array, append
from sknn.mlp import Classifier, Layer, Convolution

# Enable logging
basicConfig(
    format='%(message)s',
    level=DEBUG,
    stream=stdout)

# Dataset is split in training and test sets based on training_examples
training_examples = 1000
cl_l_files = glob('./dataset_B_Eye_Images/closedLeftEyes/*.jpg')
cl_r_files = glob('./dataset_B_Eye_Images/closedRightEyes/*.jpg')
op_l_files = glob('./dataset_B_Eye_Images/openLeftEyes/*.jpg')
op_r_files = glob('./dataset_B_Eye_Images/openRightEyes/*.jpg')


# Read files from filenames and return array of images
def parseImages(filenames):
    images = []
    for filename in filenames:
        im = Image.open(filename)
        pixels = list(im.getdata())
        images.append(pixels)
    return images


# Get dataset from images
def prepare_dataset(training=True):
    if training:
        start = 0
        end_cl = training_examples
        end_op = training_examples
    else:
        start = training_examples
        end_cl = len(cl_l_files)
        end_op = len(op_l_files)

    eye_images = []

    # Add closed eyes images for both eyes
    cl_l_imgs = parseImages(cl_l_files[start:end_cl])
    cl_r_imgs = parseImages(cl_r_files[start:end_cl])
    eye_images += cl_l_imgs
    eye_images += cl_r_imgs

    # Add open eyes images for both eyes
    op_l_imgs = parseImages(op_l_files[start:end_op])
    op_r_imgs = parseImages(op_r_files[start:end_op])
    eye_images += op_l_imgs
    eye_images += op_r_imgs

    return array(eye_images)


# Train neural network and save it in file
def train_eyes():
    dataset = prepare_dataset()
    labels = array([0]*2*training_examples + [1]*2*training_examples)

    nn = Classifier(
        layers=[
            Convolution('Rectifier', channels=8, kernel_shape=(8, 8)),
            Layer('Softmax')],
        learning_rate=0.00001,
        n_iter=5)

    nn.fit(dataset, labels)
    dump(nn, open('./blinkdata/blinkNN.pkl', 'wb'))
    return nn


# Test neural net
def test_eyes_nn(nn, training_set=False):
    # Get remaining files not on training set
    dataset = prepare_dataset(training=training_set)

    # Predict on dataset
    results = nn.predict(dataset)

    # Obtain how many open eyes are in the dataset
    if training_set:
        open_len = 2*training_examples
    else:
        open_len = 2*len(op_l_files[training_examples+1:])

    # Count correct predictions
    correct_op = 0
    correct_cl = 0
    for res in results[:open_len]:
        # First half should've returned closed
        if res == 0:
            correct_cl += 1
    for res in results[open_len+1:]:
        # Second half should've returned open
        if res == 1:
            correct_op += 1

    if training_set:
        # print 'Accuracy on training set: ' + str(float(correct)/len(results))
        print 'Accuracy on training set for closed eyes: ',
        print str(float(correct_cl)/(0.5*len(results)))
        print 'Accuracy on training set for open eyes: ',
        print str(float(correct_op)/(0.5*len(results)))
    else:
        # print 'Accuracy on test set: ' + str(float(correct)/len(results))
        print 'Accuracy on test set for closed eyes: ',
        print str(float(correct_cl)/(0.5*len(results)))
        print 'Accuracy on test set for open eyes: ',
        print str(float(correct_op)/(0.5*len(results)))
    # print 'Closed: ' + str(len(filter(lambda x: x == 0, results)))
    # print 'Opened: ' + str(len(filter(lambda x: x == 1, results)))


# Train for both eyes
nn = train_eyes()

# Run tests
test_eyes_nn(nn, training_set=True)
test_eyes_nn(nn, training_set=False)
