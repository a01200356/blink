# -*- coding: utf-8 -*-
from time import time, sleep
from math import sqrt
from numpy import array, hstack
from cv2 import VideoCapture, cvtColor, resize, COLOR_BGR2GRAY, imshow, waitKey
from dlib import get_frontal_face_detector, shape_predictor


# Wrapper for webcam reading and eye detection.
class EyeDetector(object):

    def __init__(self, input_file=0):
        self.__video_capture = VideoCapture(input_file)
        self.frames = []
        self.frame_duration = 1.0/12
        self.n_frames = 0
        self.eye_ratio = None
        self.current_eyes = None

    # Euclidean distance
    def __distance(self, p1, p2):
        return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2))

    # Get eye's height proportional to its width
    def __eye_ratio(self, eye):
        height = self.__distance(eye[5], eye[1]) + \
                 self.__distance(eye[4], eye[2])
        width = self.__distance(eye[3], eye[0])
        return height / float(2*width)

    # Return 24x24 image of eye
    def __get_eye_image(self, eye, image):
        width = eye[3].x - eye[0].x
        padding = width/4
        top = eye[0].y - width/2
        eye_img = image[top-padding:top+width+padding,
                        eye[0].x-padding:eye[3].x+padding]
        return array(resize(eye_img, (24, 24)))

    # Reads video stream and on each frame executes callback parameter
    def read_stream(self, callback):
        predictor_path = './blinkdata/shape_predictor_68_face_landmarks.dat'
        face_detector = get_frontal_face_detector()
        face_predictor = shape_predictor(predictor_path)

        while self.__video_capture.isOpened():
            start_time = time()
            ret, frame = self.__video_capture.read()
            gray = cvtColor(frame, COLOR_BGR2GRAY)
            gray = resize(gray, (400, 300))

            detections = face_detector(gray, 0)
            # For each detected face
            for k, d in enumerate(detections):
                shape = face_predictor(gray, d)  # Get coordinates
                right_eye = shape.parts()[36:42]
                left_eye = shape.parts()[42:48]

                # Get eye ratios for each eye and average them out
                eye_ratio_r = self.__eye_ratio(right_eye)
                eye_ratio_l = self.__eye_ratio(left_eye)
                self.eye_ratio = (eye_ratio_r + eye_ratio_l) / 2
                self.frames.append(self.eye_ratio)
                # print self.eye_ratio

                # Get images for each eye
                eye_img_r = self.__get_eye_image(right_eye, gray)
                eye_img_l = self.__get_eye_image(left_eye, gray)
                self.current_eyes = array([eye_img_r, eye_img_l])

                # Uncomment to show eye's captured image
                # imshow("Ojo, mucho ojo", eye_img_l)
                # waitKey(0)
                # imshow("Ojo, mucho ojo", eye_img_r)
                # waitKey(0)

                # Run caller's defined behaviour
                callback(self)

                self.n_frames += 1
                break  # only read first face found

            # Force a frame rate
            sleep_time = self.frame_duration - (time() - start_time)
            if sleep_time > 0:
                sleep(sleep_time)

    # When everything done, release the capture
    def close(self):
        self.__video_capture.release()
