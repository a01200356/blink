# -*- coding: utf-8 -*-
import sys
from os import devnull
from sys import stdout
from video import EyeDetector
from pickle import load

from blinkdata.keys import key_list


# Blink detector implementation based on image recognition
class BlinkDetector(object):

    def __init__(self):
        self.closed = False
        self.keys_per_second = 2
        self.frame_duration = 1.0/12
        self.frames_per_key = 1.0/(self.keys_per_second*self.frame_duration)
        self.blink_duration = (2.0/12, 8.0/12)
        self.blink_frames = tuple(
                map(lambda x: x*1.0/self.frame_duration, self.blink_duration))
        self.frame_cnt = 0
        self.i = 0
        self.key = 1
        self.threshold = 0.65
        self.nn = load(open('./blinkdata/blinkNN.pkl', 'rb'))

    # Method executed on each video frame
    def read_video(self, video):
        # Dirty hack to hide sknn's prints
        temp_stdout = sys.stdout
        sys.stdout = open(devnull, 'w')
        # Predict
        probabilities = self.nn.predict_proba(video.current_eyes)
        sys.stdout = temp_stdout

        # Avergae probabilities that both eyes are either open or closed
        closed_prob = (probabilities[0][0] + probabilities[1][0]) / 2
        open_prob = (probabilities[0][1] + probabilities[1][1]) / 2

        if self.closed and open_prob >= self.threshold:
            # Eye has been opened
            # print "Open"
            self.closed = False
            if self.frame_cnt >= self.blink_frames[0] and \
               self.frame_cnt <= self.blink_frames[1]:
                # Blink detected
                # print "Blink"
                j = (self.i - 1) % len(key_list)
                stdout.write('\b'*len(key_list[j][0]))
                stdout.write(' '*len(key_list[j][0]))
                stdout.write('\b'*len(key_list[j][0]))
                stdout.write(key_list[self.key][1])
                stdout.flush()
                self.i = 0
                self.key = 1
                video.frames = []
                video.n_frames = 1
        elif not self.closed and closed_prob >= self.threshold:
            # Eye has been closed
            # print "Closed"
            self.closed = True
            self.frame_cnt = 1
            self.key = (self.i - 1) % len(key_list)
        elif not self.closed and video.n_frames % self.frames_per_key == 0:
            # Show new key
            if self.i != 0:
                j = (self.i - 1) % len(key_list)
                stdout.write('\b'*len(key_list[j][0]))
                stdout.write(' '*len(key_list[j][0]))
                stdout.write('\b'*len(key_list[j][0]))
            stdout.write(key_list[self.i][0])
            stdout.flush()
            self.i = (self.i+1) % len(key_list)
        elif self.closed:
            # Eye is still closed
            self.frame_cnt += 1


eye = EyeDetector()
blink = BlinkDetector()
eye.read_stream(blink.read_video)
