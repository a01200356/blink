# -*- coding: utf-8 -*-
from numpy import float32, std
from cv2 import (kmeans, TERM_CRITERIA_EPS, TERM_CRITERIA_MAX_ITER,
                 KMEANS_RANDOM_CENTERS)

from video import EyeDetector

output_file = './blinkdata/blinkKM'
open_tests = './blinkdata/open_eye_ratio_tests.txt'
closed_tests = './blinkdata/closed_eye_ratio_tests.txt'
training_length = 10


# Training based on eye aspect ratios using on K-means
def train_clusters(video):
    training_frames = 1.0/video.frame_duration * training_length

    if video.n_frames == 0:
        # Start collecting data
        print "Look here..."
    elif video.n_frames == training_frames/2:
        # Halfway through collection of data
        print("Close your eyes for aprox. " +
              str(training_length/2) + " seconds...")
    elif video.n_frames == training_frames:
        # Start training with kmeans
        criteria = (TERM_CRITERIA_EPS + TERM_CRITERIA_MAX_ITER, 10, 1.0)
        flags = KMEANS_RANDOM_CENTERS
        frames = float32(video.frames)
        compact, labels, centers = kmeans(frames, 2, criteria, 10, flags)
        clusters = map(lambda a: a[0], centers.tolist())

        # Get values that belong to each cluster
        op_list = []
        cl_list = []
        op_index = clusters.index(max(clusters))
        cl_index = clusters.index(min(clusters))
        for i in range(0, len(frames)):
            if labels[i] == cl_index:
                cl_list.append(frames[i])
            else:
                op_list.append(frames[i])
        op_list = float32(op_list)
        cl_list = float32(cl_list)

        # Get the thresholds as center plus either 1 or 2 standard deviations
        cl_std = std(cl_list, ddof=1)
        op_std = std(op_list, ddof=1)
        op_threshold = clusters[op_index] - 2*op_std
        cl_threshold = clusters[cl_index] + cl_std

        # Output eye ratio thresholds if correct
        output = None
        if cl_threshold < op_threshold:
            print str(cl_threshold) + ',' + str(op_threshold)
            with open(output_file, 'w') as f:
                f.write(str(cl_threshold) + ',' + str(op_threshold))
            print "Ok."
        else:
            print "Something weird happened. Please try again."

        video.close()


# Test accuracy of training on test set
def test_clusters(cl_threshold, op_threshold):
    # Open eyes tests
    correct_op = 0
    tests_op = 0
    with open(open_tests) as f:
        for row in f:
            eye_ratio = float(row)
            tests_op += 1
            if eye_ratio >= op_threshold:
                correct_op += 1
    print 'Accuracy on test set for open eyes: ',
    print str(float(correct_op)/tests_op)

    # Closed eyes tests
    correct_cl = 0
    tests_cl = 0
    with open(closed_tests) as f:
        for row in f:
            eye_ratio = float(row)
            tests_cl += 1
            if eye_ratio <= cl_threshold:
                correct_cl += 1
    print 'Accuracy on test set for closed eyes: ',
    print str(float(correct_cl)/tests_cl)


eye = EyeDetector()
eye.read_stream(train_clusters)
# Read calculated thresholds and test their performance
try:
    with open(output_file, 'r') as f:
        cl, op = tuple(map(lambda x: float(x), f.readline().split(',')))
        test_clusters(cl, op)
except:
    print output_file + " not found. Try training again to generate that file."
