# Blink  

Blink detection prototypes.  
Currently two approaches are being tested for blink detection:  
 * K-means clustering of eye aspect ratios.  
 * Convolutional neural networks on grayscale eye images.  
Eventually, the best approach will be chosen for a blink based text input application.  

## System requirements  
 * Tested only on Debian and derivatives.  
 * Runs on python 2.7 only.  
 * Any webcam.  
 * Required libraries are written in setup.sh file.  

## Instructions  
### How to install  
 * Clone this repo.  
 * Make sure you have python 2.7 and virtualenv.  
 * Run setup.sh (requires root permission to install some dependencies).  

### How to run blink detector  
 * Make sure you are on your virtual environment.  
 * Run either `python blink_kmeans.py` for k-means based detector OR `python blink_neural.py` for neural network based detector.  
 * Blink or just close your eyes for a while. It should detect open/close eyes and blinks (short open/close sequences) somewhat accurately.  

### How to train with k-means  
 * Make sure you are on your virtual environment.  
 * Run `python kmeans_training.py`.  
 * Follow the written instructions.  
 * If an error message is printed at the end instead of OK, run again.  

### How to train with neural networks  
 * Download the closed eyes dataset [here](http://parnec.nuaa.edu.cn/xtan/data/datasets/dataset_B_Eye_Images.rar).  
 * Extract the contents and move them to the same directory as this code.  
 * Make sure you are on your virtual environment.  
 * Run `python neural_training.py`.  
