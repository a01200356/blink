# Install and setup all necessary dependencies
# Tested on Ubuntu 14.04
# Requires root permission

# OpenCV can only be installed globally at this time
sudo apt-get install python-opencv gfortran libblas-dev liblapack-dev

# Create virtualenv
virtualenv env
source ./env/bin/activate

# Copy globally installed opencv to virtualenv
cp -a /usr/lib/python2.7/dist-packages/cv* ./env/lib/python2.7/site-packages/

# Install python requirements
pip install numpy dlib scipy scikit-neuralnetwork theano==0.7